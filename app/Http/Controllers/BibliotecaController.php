<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Autores;
use App\Models\Livros;
use App\Models\Editoras;

class BibliotecaController extends Controller
{
    public function dados(){
        $livro=Livros::all();
        $editora=Editoras::all();
        $autor=Autores::all();
        return view('listagemLivros',['livro'=>$livro,'editora'=>$editora, 'autor'=>$autor]);
    }

    public function home(){

        return view('welcome');
    }



}
