<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Livros</title>
    <style>
    #pagina{
        position:block;
        margin-left:auto;
        margin-right:auto;
        width:auto;
        text-align: center;
        background-color: whitesmoke;

    }
    table{
        font-weight=900;
        position:block;
        margin-left:auto;
        margin-right:auto;
        background-color:gray;
        border-radius:10px;
        border: 4px red;

    }
    button{
     background-color: red;

    }
    h1{
        color: red;

    }
    </style>
</head>
<body>
<div id="pagina">
<a href="/"><button>Clique aqui para voltar para a página inicial</button></a>
<h1>Livros</h1>
<table border=2px>
<tr>
<td>ID do livro</td>
<td>Nome do livro</td>
<td>ID do autor</td>
<td>ID da editora</td>
</tr>
@foreach ($livro as $livro)
<tr>
<td>{{$livro->id}}</td> <td>{{$livro->nome}}</td>  <td>{{$livro->id_autor}}</td> <td>{{$livro->id_editora}}</td> 
</tr>
@endforeach
</table>
<h1>Editoras</h1>
<table border=2px>
<tr>
<td>ID da editora</td>
<td>Nome da editora</td>
</tr>
@foreach ($editora as $editora)
<tr>
<td>{{$editora->id}}</td> <td>{{$editora->editora}}</td>
</tr>
@endforeach
</table>
<h1>Autores</h1>
<table border=2px>
<tr>
<td>ID do autor</td>
<td>Nome do autor</td>
</tr>
@foreach ($autor as $autor)
<tr>
<td>{{$autor->id}}</td> <td>{{$autor->autor}}</td>
</tr>
@endforeach
</table>
</div>
</body>
</html>